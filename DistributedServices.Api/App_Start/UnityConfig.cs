using System;
using Domain.Entities;
using Microsoft.Practices.Unity;
using Infrastructure.Common.Mappings;
using Infrastructure.Common.Caching;
using Infrastructure.Common.Configuration;
using DistributedServices.Entities;
using Microsoft.Practices.Unity.InterceptionExtension;
using Infrastructure.Common.Logging;
using Infrastructure.Data.MainModule;
using Application.Services;
using Infrastructure.Data.MainModule;
using Infrastructure.Data.MainModule.Repositories;
using System.Collections.Generic;
using Application.Services.Results;
using Infrastructure.Common.Mappings.Mappers;

namespace DistributedServices.Api.App_Start
{
	/// <summary>
	/// Specifies the Unity configuration for the main container.
	/// </summary>
	public class UnityConfig
	{
		#region Unity Container
		private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
		{
			var container = new UnityContainer();
			RegisterTypes(container);
			return container;
		});

		/// <summary>
		/// Gets the configured Unity container.
		/// </summary>
		public static IUnityContainer GetConfiguredContainer()
		{
			return container.Value;
		}
		#endregion

		/// <summary>Registers the type mappings with the Unity container.</summary>
		/// <param name="container">The unity container to configure.</param>
		/// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
		/// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
		public static void RegisterTypes(IUnityContainer container)
		{
			//container.RegisterTypes(
			//   AllClasses.FromAssembliesInBasePath(),
			//   WithMappings.FromMatchingInterface,
			//   WithName.Default,
			//   WithLifetime.ContainerControlled);

			container.AddNewExtension<Interception>();

			container.RegisterType<IMapper<OverallIndividualResult, OverallIndividualResultDto>, AutoMapper<OverallIndividualResult, OverallIndividualResultDto>>();
			container.RegisterType<IMapper<OverallTeamResult, OverallTeamResultDto>, AutoMapper<OverallTeamResult, OverallTeamResultDto>>();
			container.RegisterType<IMapper<IndividualRacerResult, IndividualRacerResultDto>, AutoMapper<IndividualRacerResult, IndividualRacerResultDto>>();
			container.RegisterType<IMapper<TeamRaceResult, TeamRaceResultDto>, AutoMapper<TeamRaceResult, TeamRaceResultDto>>();

			container.RegisterType<IMapperProxy, MapperProxy>();

            //container.RegisterType<ICache<LeagueDto>, MemoryCache<LeagueDto>>(new Interceptor<InterfaceInterceptor>(), new InterceptionBehavior<LoggingInterceptBehavior>());
            //container.RegisterType<ICache<SeasonDto>, MemoryCache<SeasonDto>>(new Interceptor<InterfaceInterceptor>(), new InterceptionBehavior<LoggingInterceptBehavior>());
            //container.RegisterType<ICache<RaceDto>, MemoryCache<RaceDto>>(new Interceptor<InterfaceInterceptor>(), new InterceptionBehavior<LoggingInterceptBehavior>());

			container.RegisterType<ICacheConfiguration, CacheConfiguration>(new Interceptor<InterfaceInterceptor>(), new InterceptionBehavior<LoggingInterceptBehavior>());
			container.RegisterType<ILoggingConfiguration, LoggingConfiguration>();
			container.RegisterType<IAppLogger, AppLogger>();

            container.RegisterType<IResultsService, ResultsService>();

            container.RegisterType<ITeamRaceResultRepository, TeamRaceResultRepository>();
            container.RegisterType<IOverallIndividualResultRepository, OverallIndividualResultRepository>();
            container.RegisterType<IOverallTeamResultRepository, OverallTeamResultRepository>();
            container.RegisterType<IIndividualRacerResultRepository, IndividualRacerResultRepository>();
		}
	}
}
