﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistributedServices.Entities
{
    public class TeamRaceResultDto
    {
        public string LeagueName { get; set; }
        public string LeagueDesc { get; set; }
        public int LeagueId { get; set; }
        public string SeasonName { get; set; }
        public string SeasonDesc { get; set; }
        public int SeasonId { get; set; }
        public string RaceName { get; set; }
        public string RaceDescription { get; set; }
        public int RaceId { get; set; }
        public string TeamShortName { get; set; }
        public string TeamFullName { get; set; }
        public int TeamId { get; set; }
        public int? TotalRacers { get; set; }
        public double? TeamPoints { get; set; }
        public string ClientToken { get; set; }
    }
}
