using Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Infrastructure.Data.MainModule.Models.Mapping
{
    public class IndividualRacerResultMap : EntityTypeConfiguration<IndividualRacerResult>
    {
        public IndividualRacerResultMap()
        {
            // Primary Key
            this.HasKey(t => new { t.LeagueName, t.LeagueDesc, t.LeagueId, t.SeasonName, t.SeasonDesc, t.SeasonId, t.RaceName, t.RaceDescription, t.RaceIsPublic, t.LastName, t.FirstName, t.DateOfBirth, t.Gender, t.RacerId, t.RaceId, t.Time, t.DisciplineDescription, t.TeamPoints, t.RacerPoints, t.AgeGroup, t.AgeGroupId, t.TeamShortName, t.TeamFullName, t.TeamId, t.ClientToken });

            // Properties
            this.Property(t => t.LeagueName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.LeagueDesc)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.LeagueId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.SeasonName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.SeasonDesc)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.SeasonId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.RaceName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.RaceDescription)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.LastName)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.FirstName)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Gender)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.RacerId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NastarId)
                .HasMaxLength(20);

            this.Property(t => t.Bib)
                .HasMaxLength(10);

            this.Property(t => t.RaceId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Discipline)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.DisciplineDescription)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.AgeGroup)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.AgeGroupId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.TeamShortName)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.TeamFullName)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.TeamId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("IndividualRacerResults");
            this.Property(t => t.LeagueName).HasColumnName("LeagueName");
            this.Property(t => t.LeagueDesc).HasColumnName("LeagueDesc");
            this.Property(t => t.LeagueId).HasColumnName("LeagueId");
            this.Property(t => t.SeasonName).HasColumnName("SeasonName");
            this.Property(t => t.SeasonDesc).HasColumnName("SeasonDesc");
            this.Property(t => t.SeasonId).HasColumnName("SeasonId");
            this.Property(t => t.RaceName).HasColumnName("RaceName");
            this.Property(t => t.RaceDescription).HasColumnName("RaceDescription");
            this.Property(t => t.RaceIsPublic).HasColumnName("RaceIsPublic");
            this.Property(t => t.RacePublicOn).HasColumnName("RacePublicOn");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.DateOfBirth).HasColumnName("DateOfBirth");
            this.Property(t => t.Gender).HasColumnName("Gender");
            this.Property(t => t.RacerId).HasColumnName("RacerId");
            this.Property(t => t.NastarId).HasColumnName("NastarId");
            this.Property(t => t.Bib).HasColumnName("Bib");
            this.Property(t => t.RaceAge).HasColumnName("RaceAge");
            this.Property(t => t.RaceId).HasColumnName("RaceId");
            this.Property(t => t.Time).HasColumnName("Time");
            this.Property(t => t.Handicap).HasColumnName("Handicap");
            this.Property(t => t.Discipline).HasColumnName("Discipline");
            this.Property(t => t.DisciplineDescription).HasColumnName("DisciplineDescription");
            this.Property(t => t.TeamPoints).HasColumnName("TeamPoints");
            this.Property(t => t.RacerPoints).HasColumnName("RacerPoints");
            this.Property(t => t.AgeGroup).HasColumnName("AgeGroup");
            this.Property(t => t.AgeGroupId).HasColumnName("AgeGroupId");
            this.Property(t => t.TeamShortName).HasColumnName("TeamShortName");
            this.Property(t => t.TeamFullName).HasColumnName("TeamFullName");
            this.Property(t => t.TeamId).HasColumnName("TeamId");
            this.Property(t => t.ClientToken).HasColumnName("ClientToken");
        }
    }
}
