﻿using Domain.Entities;
using Infrastructure.Data.MainModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Data.MainModule.Repositories
{
    public class OverallTeamResultRepository : IOverallTeamResultRepository
    {
        private readonly LeagueContext _context;

        public OverallTeamResultRepository()
        {
            _context = new LeagueContext();
        }

        public OverallTeamResult Add(OverallTeamResult item)
        {
            return null;
        }

        public OverallTeamResult Update(OverallTeamResult item)
        {
            return null;
        }

        public IEnumerable<OverallTeamResult> GetBy(Func<OverallTeamResult, bool> predicate)
        {
            var items = _context.OverallTeamResults.Where(predicate);

            return items;
        }

        public IEnumerable<OverallTeamResult> GetAll()
        {
            return null;
        }

        public List<OverallTeamResult> GetManyBy(Func<OverallTeamResult, bool> predicate)
        {
            var items = _context.OverallTeamResults.Where(predicate);

            return items.ToList();
        }

        OverallTeamResult IRepository<OverallTeamResult>.GetBy(Func<OverallTeamResult, bool> predicate)
        {
            throw new NotImplementedException();
        }

        public OverallTeamResult Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}
