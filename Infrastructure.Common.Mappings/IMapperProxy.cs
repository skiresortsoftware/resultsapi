﻿namespace Infrastructure.Common.Mappings
{
	public interface IMapperProxy
	{
		TResult Map<TSource,TResult>(TSource obj) where TSource : class;
	}
}