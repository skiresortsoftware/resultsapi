﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public partial class OverallIndividualResult
    {
        public string LeagueName { get; set; }
        public string LeagueDesc { get; set; }
        public int LeagueId { get; set; }
        public string SeasonName { get; set; }
        public string SeasonDesc { get; set; }
        public int SeasonId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Gender { get; set; }
        public int RacerId { get; set; }
        public string Bib { get; set; }
        public string Discipline { get; set; }
        public string DisciplineDescription { get; set; }
        public double? TotalPoints { get; set; }
        public string AgeGroup { get; set; }
        public int AgeGroupId { get; set; }
        public string TeamShortName { get; set; }
        public string TeamFullName { get; set; }
        public int TeamId { get; set; }
        public Guid ClientToken { get; set; }
    }
}
