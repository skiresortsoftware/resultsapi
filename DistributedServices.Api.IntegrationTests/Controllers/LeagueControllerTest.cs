﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DistributedServices.Api.Controllers;
using DistributedServices.Entities;
using System.Collections.Generic;
using Infrastructure.Common.Mappings;
using Infrastructure.Common.Caching;
using Infrastructure.Common.Configuration;
using Infrastructure.Data.MainModule;
using Application.Services;

namespace DistributedServices.Api.IntegrationTests.Controllers
{
	[TestClass]
	public class LeagueControllerTest
	{
        //#region Fields

        //private readonly IMapper<Domain.Entities.League, LeagueDto> _mapper;

        //private readonly ICache<LeagueDto> _cache;

        //private readonly ICacheConfiguration _cacheConfiguration;

        //private readonly ILeagueService _leagueService;

        //private readonly ILeagueRepository _leagueRepository;

        //private Domain.Entities.League _league = new Domain.Entities.League()
        //{
        //    ClientToken = Guid.NewGuid(),
        //    DateCreated = DateTime.Now,
        //    Description = "Test Description",
        //    Id = 1,
        //};

        //private LeagueDto _leagueDto = new LeagueDto()
        //{
        //    ClientToken = Guid.NewGuid().ToString(),
        //    Description = "Test Description",
        //    Id = 1,
        //};

        //private List<Domain.Entities.League> _leagues = new List<Domain.Entities.League>()
        //{
        //    new Domain.Entities.League 
        //    {
        //        ClientToken = Guid.NewGuid(),
        //        DateCreated = DateTime.Now,
        //        Description = "Test Description",
        //        Id = 1,
        //    },
        //    new Domain.Entities.League 
        //    {
        //        ClientToken = Guid.NewGuid(),
        //        DateCreated = DateTime.Now,
        //        Description = "Test Description2",
        //        Id = 12,
        //    }
        //};

        //private List<LeagueDto> _leagueDtos = new List<LeagueDto>()
        //{
        //    new LeagueDto 
        //    {
        //        ClientToken = Guid.NewGuid().ToString(),
        //        Description = "Test Description",
        //        Id = 1,
        //    },
        //    new LeagueDto 
        //    {
        //        ClientToken = Guid.NewGuid().ToString(),
        //        Description = "Test Description2",
        //        Id = 12,
        //    }
        //};

        //#endregion

        //#region Controllers

        //public LeagueControllerTest()
        //{
        //    _leagueRepository = new LeagueRepository();

        //    _leagueService = new LeagueService(_leagueRepository);

        //    _mapper = new LeagueMapper(null);

        //    _cacheConfiguration = new CacheConfiguration();

        //    _cache = new MemoryCache<LeagueDto>(_cacheConfiguration);
        //}

        //#endregion

        //#region Test Methods

        ////[TestMethod]
        ////public void GetAllBy_WithClientTokenAndObj_ReturnsLeagueDtos()
        ////{
        ////	// Arrange
        ////	var clientToken = "8384DDA6-371E-40A3-B343-4D061B00B52C";

        ////	var obj = "Test";

        ////	var leagueController = new LeagueController(_leagueService, _mapper, _cache);

        ////	// Act
        ////	var getAllByResponse = leagueController.GetAllBy(clientToken, obj);

        ////	// Assert
        ////	Assert.IsInstanceOfType(getAllByResponse, typeof(List<LeagueDto>));
        ////}

        ////[TestMethod]
        ////public void GetBy_WithClientTokenObjAndId_ReturnsLeagueDto()
        ////{
        ////	// Arrange
        ////	var clientToken = "8384DDA6-371E-40A3-B343-4D061B00B52C";

        ////	var obj = "Test";

        ////	var id = 2;

        ////	var leagueController = new LeagueController(_leagueService, _mapper, _cache);

        ////	// Act
        ////	var getByResponse = leagueController.GetBy(clientToken, obj, id);

        ////	// Assert
        ////	Assert.IsInstanceOfType(getByResponse, typeof(LeagueDto));
        ////}

        ////[TestMethod]
        ////public void Post_WithLeagueDto_ReturnsLeagueDto()
        ////{
        ////	// Arrange
        ////	var clientToken = "8384DDA6-371E-40A3-B343-4D061B00B52C";

        ////	var leagueController = new LeagueController(_leagueService, _mapper, _cache);

        ////	// Act
        ////	var postResponse = leagueController.Post(_leagueDto, clientToken);

        ////	// Assert
        ////	Assert.IsInstanceOfType(postResponse, typeof(LeagueDto));
        ////}

        ////[TestMethod]
        ////public void Put_WithLeagueDto_ReturnsLeagueDto()
        ////{
        ////	// Arrange
        ////	var clientToken = "8384DDA6-371E-40A3-B343-4D061B00B52C";

        ////	var id = 2;

        ////	var leagueController = new LeagueController(_leagueService, _mapper, _cache);

        ////	// Act
        ////	var putResponse = leagueController.Put(clientToken, id, _leagueDto);

        ////	// Assert
        ////	Assert.IsInstanceOfType(putResponse, typeof(LeagueDto));
        ////}

        ////[TestMethod]
        ////public void Delete_WithId_ReturnsLeagueDto()
        ////{
        ////	// Arrange
        ////	var clientToken = "8384DDA6-371E-40A3-B343-4D061B00B52C";

        ////	var id = 2;

        ////	var leagueController = new LeagueController(_leagueService, _mapper, _cache);

        ////	// Act
        ////	var deleteResponse = leagueController.Delete(clientToken, id);

        ////	// Assert
        ////	Assert.IsInstanceOfType(deleteResponse, typeof(LeagueDto));
        ////}

        //#endregion
	}
}
