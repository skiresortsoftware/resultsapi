﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Data.MainModule.Mapping
{
    public class OverallTeamResultMap : EntityTypeConfiguration<OverallTeamResult>
    {
        public OverallTeamResultMap()
        {
            // Primary Key
            this.HasKey(t => new { t.LeagueName, t.LeagueDesc, t.LeagueId, t.SeasonName, t.SeasonDesc, t.SeasonId, t.TeamShortName, t.TeamFullName, t.TeamId, t.ClientToken });

            // Properties
            this.Property(t => t.LeagueName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.LeagueDesc)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.LeagueId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.SeasonName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.SeasonDesc)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.SeasonId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.TeamShortName)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.TeamFullName)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.TeamId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("OverallTeamResults");
            this.Property(t => t.LeagueName).HasColumnName("LeagueName");
            this.Property(t => t.LeagueDesc).HasColumnName("LeagueDesc");
            this.Property(t => t.LeagueId).HasColumnName("LeagueId");
            this.Property(t => t.SeasonName).HasColumnName("SeasonName");
            this.Property(t => t.SeasonDesc).HasColumnName("SeasonDesc");
            this.Property(t => t.SeasonId).HasColumnName("SeasonId");
            this.Property(t => t.TeamShortName).HasColumnName("TeamShortName");
            this.Property(t => t.TeamFullName).HasColumnName("TeamFullName");
            this.Property(t => t.TeamId).HasColumnName("TeamId");
            this.Property(t => t.TotalPoints).HasColumnName("TotalPoints");
            this.Property(t => t.ClientToken).HasColumnName("ClientToken");
        }
    }
}
