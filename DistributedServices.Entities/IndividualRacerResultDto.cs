﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistributedServices.Entities
{
    public class IndividualRacerResultDto
    {
        public string LeagueName { get; set; }
        public string LeagueDesc { get; set; }
        public int LeagueId { get; set; }
        public string SeasonName { get; set; }
        public string SeasonDesc { get; set; }
        public int SeasonId { get; set; }
        public string RaceName { get; set; }
        public string RaceDescription { get; set; }
        public bool RaceIsPublic { get; set; }
        public DateTime? RacePublicOn { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Gender { get; set; }
        public int RacerId { get; set; }
        public string NastarId { get; set; }
        public string Bib { get; set; }
        public int? RaceAge { get; set; }
        public int RaceId { get; set; }
        public double Time { get; set; }
        public double? Handicap { get; set; }
        public string Discipline { get; set; }
        public string DisciplineDescription { get; set; }
        public double TeamPoints { get; set; }
        public double RacerPoints { get; set; }
        public string AgeGroup { get; set; }
        public int AgeGroupId { get; set; }
        public string TeamShortName { get; set; }
        public string TeamFullName { get; set; }
        public int TeamId { get; set; }
        public string ClientToken { get; set; }
    }
}
