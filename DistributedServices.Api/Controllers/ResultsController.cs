﻿using System.Linq;
using AutoMapper;
using Domain.Entities;
using Infrastructure.Common.Caching;
using Infrastructure.Common.Mappings;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using DistributedServices.Entities;
using Application.Services.Results;

namespace DistributedServices.Api.Controllers
{
	[EnableCors("*", "*", "*")]
    [RoutePrefix("api")]
	public class ResultsController : ApiController
	{
		private readonly IResultsService _service;

		private readonly IMapper<IndividualRacerResult, IndividualRacerResultDto> _indRacerResultsMapper;
		
		private readonly IMapper<TeamRaceResult, TeamRaceResultDto> _tmRaceResultsMapper;
		
		private readonly IMapper<OverallIndividualResult, OverallIndividualResultDto> _oaIndResultsMapper;
		
		private readonly IMapper<OverallTeamResult, OverallTeamResultDto> _oaTeamResultsMapper;

		private readonly IMapperProxy _mapper;

        private readonly ICache<IndividualRacerResultDto> _cache;

		private const string CacheIdFormat = "league-{0}-{1}";

        public ResultsController(IResultsService service, 
								 IMapper<IndividualRacerResult, IndividualRacerResultDto> indRacerResultsMapper,
                                 IMapper<TeamRaceResult, TeamRaceResultDto> tmRaceResultsMapper,
                                 IMapper<OverallIndividualResult, OverallIndividualResultDto> oaIndResultsMapper,
                                 IMapper<OverallTeamResult, OverallTeamResultDto> oaTeamResultsMapper,
								 IMapperProxy mapper)
		{
			_service = service;

	        _mapper = mapper;

			_indRacerResultsMapper = indRacerResultsMapper;

			_tmRaceResultsMapper = tmRaceResultsMapper;

			_oaIndResultsMapper = oaIndResultsMapper;

			_oaTeamResultsMapper = oaTeamResultsMapper;
		}

		[Route("seasons/{seasonId}/results/overall/team")]
		public List<OverallTeamResultDto> GetOverallTeamResults(int seasonId)
		{
			var items = _service.GetOverallTeamResults(seasonId);

			var mappedItems = _oaTeamResultsMapper.Map(items);

            return mappedItems;
		}

        [Route("seasons/{seasonId}/results/team")]
        public List<TeamRaceResultDto> GetOverallTeamResults(int seasonId, int teamId)
        {
            var items = _service.GetAllTeamRaceResults(seasonId, teamId).ToList();

            var mappedItems = _tmRaceResultsMapper.Map(items);

            return mappedItems;
        }

        [Route("seasons/{seasonId}/results/overall/individual")]
        public List<OverallIndividualResultDto> GetOverallIndividualResults(int seasonId, string gender = "", string discipline = "")
        {
            var items = _service.GetOverallIndividualResults(seasonId);

            if (gender != string.Empty)
                items = items.Where(i => i.Gender.ToLower() == gender.ToLower()).ToList();

            if (discipline != string.Empty)
                items = items.Where(i => i.Discipline.ToLower() == discipline.ToLower()).ToList();

            var mappedItem = _oaIndResultsMapper.Map(items).ToList();

            return mappedItem;
        }

        [Route("races/{raceId}/results/individual")]
        public List<IndividualRacerResultDto> GetIndividualRacerResults(int raceId, string gender = "", string discipline = "")
        {
            var items = _service.GetIndividualRacerResults(raceId);

            if (gender != string.Empty)
                items = items.Where(i => i.Gender.ToLower() == gender.ToLower()).ToList();

            if (discipline != string.Empty)
                items = items.Where(i => i.Discipline.ToLower() == discipline.ToLower()).ToList();

            var mappedItem = _indRacerResultsMapper.Map(items).ToList();

            return mappedItem;
        }

        [Route("races/{raceId}/results/individual")]
        public List<IndividualRacerResultDto> GetIndividualRacerResults(int raceId, int teamId)
        {
            var items = _service.GetIndividualRacerResults(raceId).Where(i => i.TeamId == teamId).ToList();

            var mappedItem = _indRacerResultsMapper.Map(items).ToList();

            return mappedItem;
        }

        [Route("races/{raceId}/results/agegroups")]
        public List<AgeGroupDto> GetAgeGroupResults(int raceId, string gender = "", string discipline = "")
        {
            var items = _service.GetIndividualRacerResults(raceId);

            if (gender != string.Empty)
                items = items.Where(i => i.Gender.ToLower() == gender.ToLower()).ToList();

            if (discipline != string.Empty)
                items = items.Where(i => i.Discipline.ToLower() == discipline.ToLower()).ToList();

            var mappedItem = _indRacerResultsMapper.Map(items).ToList();

            var ageGroups = mappedItem.GroupBy(a => a.AgeGroup).ToList();

            var ageGroupDtos = new List<AgeGroupDto>();

            foreach (var ageGroup in ageGroups)
            {
                var indRacerResults = mappedItem.Where(a => a.AgeGroup == ageGroup.Key).ToList();

                var ageGroupDto = new AgeGroupDto
                {
                    AgeGroup = ageGroup.Key,
                    IndividualRacerResult = indRacerResults.ToList()
                };

                ageGroupDtos.Add(ageGroupDto);
            }

            return ageGroupDtos;
        }

        [Route("races/{raceId}/results/team")]
        public List<TeamRaceResultDto> GetTeamRaceResults(int raceId)
        {
            var items = _service.GetTeamRaceResults(raceId);

            var mappedItem = _tmRaceResultsMapper.Map(items).ToList();

            return mappedItem;
        }



	}
}
