﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class ReportIndividualTeamPoint
    {
        public string LeagueName { get; set; }
        public string LeagueDesc { get; set; }
        public string SeasonName { get; set; }
        public string SeasonDesc { get; set; }
        public string RaceName { get; set; }
        public string RaceDescription { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int? RaceAge { get; set; }
        public string Discipline { get; set; }
        public string Description { get; set; }
        public double Points { get; set; }
        public int RaceId { get; set; }
        public int TeamId { get; set; }
        public int RacerId { get; set; }
        public int Position { get; set; }
        public string AgeGroup { get; set; }
        public int AgeGroupId { get; set; }
        public string TeamShortName { get; set; }
        public string TeamFullName { get; set; }
    }
}
