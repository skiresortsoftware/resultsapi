﻿using Domain.Entities;
using Infrastructure.Data.MainModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Data.MainModule.Repositories
{
    public class OverallIndividualResultRepository : IOverallIndividualResultRepository
    {
        private readonly LeagueContext _context;

        public OverallIndividualResultRepository()
        {
            _context = new LeagueContext();
        }

        public OverallIndividualResult Add(OverallIndividualResult item)
        {
            return null;
        }

        public OverallIndividualResult Update(OverallIndividualResult item)
        {
            return null;
        }

        public OverallIndividualResult GetBy(Func<OverallIndividualResult, bool> predicate)
        {
            var items = _context.OverallIndividualResults.FirstOrDefault(predicate);

            return items;
        }

        public IEnumerable<OverallIndividualResult> GetAll()
        {
            return null;
        }

        public List<OverallIndividualResult> GetManyBy(Func<OverallIndividualResult, bool> predicate)
        {
            var items = _context.OverallIndividualResults.Where(predicate);

            return items.ToList();
        }

        public OverallIndividualResult Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}
