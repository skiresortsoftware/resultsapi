﻿using Domain.Entities;
using Infrastructure.Data.MainModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Data.MainModule.Repositories
{
    public class TeamRaceResultRepository : ITeamRaceResultRepository
    {
        private readonly LeagueContext _context;

        public TeamRaceResultRepository()
        {
            _context = new LeagueContext();
        }

        public TeamRaceResult Add(TeamRaceResult item)
        {
            return null;
        }

        public TeamRaceResult Update(TeamRaceResult item)
        {
            return null;
        }

        public TeamRaceResult GetBy(Func<TeamRaceResult, bool> predicate)
        {
            var items = _context.TeamRaceResults.FirstOrDefault(predicate);

            return items;
        }

        public IEnumerable<TeamRaceResult> GetAll()
        {
            return null;
        }

        public List<TeamRaceResult> GetManyBy(Func<TeamRaceResult, bool> predicate)
        {
            var items = _context.TeamRaceResults.Where(predicate);

            return items.ToList();
        }

        public TeamRaceResult Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}
