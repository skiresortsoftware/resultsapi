﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Results
{
    public interface IResultsService
    {
        List<OverallTeamResult> GetOverallTeamResults(int seasonId);
        List<OverallIndividualResult> GetOverallIndividualResults(int seasonId);
        List<IndividualRacerResult> GetIndividualRacerResults(int raceId);
        List<TeamRaceResult> GetTeamRaceResults(int raceId);
        List<Domain.Entities.TeamRaceResult> GetAllTeamRaceResults(int seasonId, int teamId);
    }
}
