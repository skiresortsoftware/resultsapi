﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Data.MainModule.Mapping
{
    public class TeamRaceResultMap : EntityTypeConfiguration<TeamRaceResult>
    {
        public TeamRaceResultMap()
        {
            // Primary Key
            this.HasKey(t => new { t.LeagueName, t.LeagueDesc, t.SeasonName, t.SeasonDesc, t.RaceName, t.RaceDescription, t.RaceId, t.TeamShortName, t.TeamFullName, t.TeamId, t.ClientToken });

            // Properties
            this.Property(t => t.LeagueName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.LeagueDesc)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.SeasonName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.SeasonDesc)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.RaceName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.RaceDescription)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.RaceId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.TeamShortName)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.TeamFullName)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.TeamId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("TeamRaceResults");
            this.Property(t => t.LeagueName).HasColumnName("LeagueName");
            this.Property(t => t.LeagueDesc).HasColumnName("LeagueDesc");
            this.Property(t => t.SeasonName).HasColumnName("SeasonName");
            this.Property(t => t.SeasonDesc).HasColumnName("SeasonDesc");
            this.Property(t => t.RaceName).HasColumnName("RaceName");
            this.Property(t => t.RaceDescription).HasColumnName("RaceDescription");
            this.Property(t => t.RaceId).HasColumnName("RaceId");
            this.Property(t => t.TeamShortName).HasColumnName("TeamShortName");
            this.Property(t => t.TeamFullName).HasColumnName("TeamFullName");
            this.Property(t => t.TeamId).HasColumnName("TeamId");
            this.Property(t => t.TotalRacers).HasColumnName("TotalRacers");
            this.Property(t => t.TeamPoints).HasColumnName("TeamPoints");
            this.Property(t => t.ClientToken).HasColumnName("ClientToken");
        }
    }
}
