using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Infrastructure.Data.MainModule.Models.Mapping;
using Domain.Entities;
using Infrastructure.Data.MainModule.Mapping;

namespace Infrastructure.Data.MainModule.Models
{
    public partial class LeagueContext : DbContext
    {
        static LeagueContext()
        {
            Database.SetInitializer<LeagueContext>(null);
        }

        public LeagueContext()
            : base("Name=LeagueContext")
        {
        }

        public DbSet<IndividualRacerResult> IndividualRacerResults { get; set; }
        public DbSet<OverallIndividualResult> OverallIndividualResults { get; set; }
        public DbSet<OverallTeamResult> OverallTeamResults { get; set; }
        //public DbSet<ReportIndividualTeamPoint> ReportIndividualTeamPoints { get; set; }
        public DbSet<TeamRaceResult> TeamRaceResults { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new IndividualRacerResultMap());
            modelBuilder.Configurations.Add(new OverallIndividualResultMap());
            modelBuilder.Configurations.Add(new OverallTeamResultMap());
            modelBuilder.Configurations.Add(new TeamRaceResultMap());
        }
    }
}
