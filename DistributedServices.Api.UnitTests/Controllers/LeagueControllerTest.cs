﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using DistributedServices.Api.Controllers;
using DistributedServices.Entities;
using Infrastructure.Common.Mappings;
using Infrastructure.Common.Caching;
using Application.Services;

namespace DistributedServices.Api.UnitTests.Controllers
{
	[TestClass]
	public class LeagueControllerTest
	{
        //#region Fields

        //private readonly Mock<IMapper<Domain.Entities.League, LeagueDto>> _mockMapper;

        //private readonly Mock<ICache<LeagueDto>> _mockCache;

        //private readonly Mock<ILeagueService> _mockLeagueService;

        //private Domain.Entities.League _league = new Domain.Entities.League()
        //    {
        //        ClientToken = Guid.NewGuid(),
        //        DateCreated = DateTime.Now,
        //        Description = "Test Description",
        //        Id = 1,
        //    };

        //private LeagueDto _leagueDto = new LeagueDto()
        //{
        //    ClientToken = Guid.NewGuid().ToString(),
        //    Description = "Test Description",
        //    Id = 1,
        //};

        //private List<Domain.Entities.League> _leagues = new List<Domain.Entities.League>()
        //{
        //    new Domain.Entities.League 
        //    {
        //        ClientToken = Guid.NewGuid(),
        //        DateCreated = DateTime.Now,
        //        Description = "Test Description",
        //        Id = 1,
        //    },
        //    new Domain.Entities.League 
        //    {
        //        ClientToken = Guid.NewGuid(),
        //        DateCreated = DateTime.Now,
        //        Description = "Test Description2",
        //        Id = 12,
        //    }
        //};

        //private List<LeagueDto> _leagueDtos = new List<LeagueDto>()
        //{
        //    new LeagueDto 
        //    {
        //        ClientToken = Guid.NewGuid().ToString(),
        //        Description = "Test Description",
        //        Id = 1,
        //    },
        //    new LeagueDto 
        //    {
        //        ClientToken = Guid.NewGuid().ToString(),
        //        Description = "Test Description2",
        //        Id = 12,
        //    }
        //};

        //#endregion

        //#region Controllers

        //public LeagueControllerTest()
        //{
        //    _mockLeagueService = new Mock<ILeagueService>();

        //    _mockMapper = new Mock<IMapper<Domain.Entities.League, LeagueDto>>();

        //    _mockMapper.Setup(m => m.Map(It.IsAny<LeagueDto>())).Returns(_league);

        //    _mockMapper.Setup(m => m.Map(It.IsAny<Domain.Entities.League>())).Returns(_leagueDto);

        //    _mockMapper.Setup(m => m.Map(It.IsAny<List<LeagueDto>>())).Returns(_leagues);

        //    _mockMapper.Setup(m => m.Map(It.IsAny<List<Domain.Entities.League>>())).Returns(_leagueDtos);

        //    _mockCache = new Mock<ICache<LeagueDto>>();

        //    _mockLeagueService.Setup(m => m.GetBy(It.IsAny<Func<Domain.Entities.League, bool>>())).Returns(_league);

        //    _mockLeagueService.Setup(m => m.Add(It.IsAny<Domain.Entities.League>())).Returns(_league);

        //    _mockLeagueService.Setup(m => m.Update(It.IsAny<Domain.Entities.League>())).Returns(_league);

        //    _mockLeagueService.Setup(m => m.Delete(It.IsAny<int>())).Returns(_league);

        //    _mockLeagueService.Setup(m => m.GetManyBy(It.IsAny<Func<Domain.Entities.League, bool>>())).Returns(_leagues);                  
        //}

        //#endregion

        //#region Test Methods

        ////[TestMethod]
        ////public void GetAllBy_WithClientTokenAndObj_ReturnsLeagueDtos()
        ////{
        ////	// Arrange
        ////	var clientToken = Guid.NewGuid().ToString();

        ////	var obj = "Test";

        ////	var leagueController = new LeagueController(_mockLeagueService.Object, _mockMapper.Object, _mockCache.Object);

        ////	// Act
        ////	var getAllByResponse = leagueController.GetAllBy(clientToken, obj);

        ////	// Assert
        ////	Assert.IsInstanceOfType(getAllByResponse, typeof(List<LeagueDto>));
        ////}

        ////[TestMethod]
        ////public void GetBy_WithClientTokenObjAndId_ReturnsLeagueDto()
        ////{
        ////	// Arrange
        ////	var clientToken = Guid.NewGuid().ToString();

        ////	var obj = "Test";

        ////	var id = 1;

        ////	var leagueController = new LeagueController(_mockLeagueService.Object, _mockMapper.Object, _mockCache.Object);

        ////	// Act
        ////	var getByResponse = leagueController.GetBy(clientToken, obj, id);

        ////	// Assert
        ////	Assert.IsInstanceOfType(getByResponse, typeof(LeagueDto));
        ////}

        ////[TestMethod]
        ////public void Post_WithHypermediaDto_ReturnsLeagueDto()
        ////{
        ////	// Arrange
        ////	var clientToken = Guid.NewGuid().ToString();

        ////	var leagueController = new LeagueController(_mockLeagueService.Object, _mockMapper.Object, _mockCache.Object);

        ////	// Act
        ////	var postResponse = leagueController.Post(_leagueDto, clientToken);

        ////	// Assert
        ////	Assert.IsInstanceOfType(postResponse, typeof(LeagueDto));
        ////}

        ////[TestMethod]
        ////public void Put_WithHypermediaDto_ReturnsLeagueDto()
        ////{
        ////	// Arrange
        ////	var clientToken = Guid.NewGuid().ToString();

        ////	var id = 1;

        ////	var leagueController = new LeagueController(_mockLeagueService.Object, _mockMapper.Object, _mockCache.Object);

        ////	// Act
        ////	var putResponse = leagueController.Put(clientToken, id, _leagueDto);

        ////	// Assert
        ////	Assert.IsInstanceOfType(putResponse, typeof(LeagueDto));
        ////}

        ////[TestMethod]
        ////public void Delete_WithId_ReturnsLeagueDto()
        ////{
        ////	// Arrange
        ////	var clientToken = Guid.NewGuid().ToString();

        ////	var id = 1;

        ////	var leagueController = new LeagueController(_mockLeagueService.Object, _mockMapper.Object, _mockCache.Object);

        ////	// Act
        ////	var deleteResponse = leagueController.Delete(clientToken, id);

        ////	// Assert
        ////	Assert.IsInstanceOfType(deleteResponse, typeof(LeagueDto));
        ////}

        //#endregion
	}
}
