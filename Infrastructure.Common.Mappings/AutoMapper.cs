﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;

namespace Infrastructure.Common.Mappings
{
    public class AutoMapper<T, U> : IMapper<T, U>
    {
        public T Map(U obj)
        {
            Mapper.CreateMap<U, T>();

            return Mapper.Map<U, T>(obj);
        }

        public U Map(T obj)
        {
            Mapper.CreateMap<T, U>();

            return Mapper.Map<T, U>(obj);
        }

        public List<T> Map(List<U> objs)
        {
            var mappedObjects = objs.Select(o => Map(o)).ToList();

            return mappedObjects;
        }

        public List<U> Map(List<T> objs)
        {
            var mappedObjects = objs.Select(o => Map(o)).ToList();

            return mappedObjects;
        }
    }
}
