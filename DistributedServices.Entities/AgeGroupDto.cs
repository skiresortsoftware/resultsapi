﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistributedServices.Entities
{
    public class AgeGroupDto
    {
        public string AgeGroup { get; set; }
        public int AgeGroupId { get; set; }
        public List<IndividualRacerResultDto> IndividualRacerResult { get; set; }
    }
}
