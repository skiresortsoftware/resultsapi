﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DistributedServices.Entities;
using System.Collections.Generic;

namespace Infrastructure.Common.Mappings.UnitTests.Hypermedia
{
    //[TestClass]
    //public class HypermediaMapperTest
    //{
    //    #region Fields

    //    private readonly Domain.Entities.League _domainObject = new Domain.Entities.League
    //    {
    //        ClientToken = Guid.NewGuid(),
    //        Description = "Test Description1",
    //        //Href = "http://www.google.com",
    //        Id = 1,
    //        //Object = "TestObject1",
    //        //ObjectId = 2,
    //        //Target = "_blank",
    //        //Text = "Test Text1"
    //    };

    //    private readonly LeagueDto _dtoObject = new LeagueDto
    //    {
    //        ClientToken = Guid.NewGuid().ToString(),
    //        Description = "Test Description1",
    //        //Href = "http://www.google.com",
    //        Id = 1,
    //        //Object = "TestObject1",
    //        //ObjectId = 2,
    //        //Target = "_blank",
    //        //Text = "Test Text1"
    //    };

    //    private readonly List<Domain.Entities.League> _domainObjects = new List<Domain.Entities.League>()
    //    {
    //            new Domain.Entities.League
    //            {
    //                ClientToken = Guid.NewGuid(),
    //                Description = "Test Description1",
    //                //Href = "http://www.google.com",
    //                Id = 1,
    //                //Object = "TestObject1",
    //                //ObjectId = 2,
    //                //Target = "_blank",
    //                //Text = "Test Text1"
    //            },
    //            new Domain.Entities.League
    //            {
    //                ClientToken = Guid.NewGuid(),
    //                Description = "Test Description2",
    //                //Href = "http://www.google.com",
    //                Id = 2,
    //                //Object = "TestObject2",
    //                //ObjectId = 3,
    //                //Target = "_blank",
    //                //Text = "Test Text2"
    //            }
    //        };


    //    private readonly List<LeagueDto> _dtoObjects = new List<LeagueDto>
    //        {
    //            new LeagueDto
    //            {
    //                ClientToken = Guid.NewGuid().ToString(),
    //                Description = "Test Description1",
    //                //Href = "http://www.google.com",
    //                Id = 1,
    //                //Object = "TestObject1",
    //                //ObjectId = 2,
    //                //Target = "_blank",
    //                //Text = "Test Text1"
    //            },
    //            new LeagueDto
    //            {
    //                ClientToken = Guid.NewGuid().ToString(),
    //                Description = "Test Description2",
    //                //Href = "http://www.google.com",
    //                Id = 2,
    //                //Object = "TestObject2",
    //                //ObjectId = 3,
    //                //Target = "_blank",
    //                //Text = "Test Text2"
    //            }
    //        };


    //    #endregion

    //    #region Test methods

    //    [TestMethod]
    //    public void Map_WithDomainObject_ReturnsClientTokenGuid()
    //    {
    //        // Arrange
    //        var mapper = new LeagueMapper(null);

    //        Guid result;

    //        // Act
    //        var dtoObject = mapper.Map(_domainObject);

    //        var isClientTokenGuid = Guid.TryParse(dtoObject.ClientToken, out result);

    //        // Assert
    //        Assert.IsTrue(isClientTokenGuid);
    //    }

    //    [TestMethod]
    //    public void Map_WithDtoObject_ReturnsDomainObject()
    //    {
    //        // Arrange
    //        var mapper = new LeagueMapper(null);

    //        // Act
    //        var domainObject = mapper.Map(_dtoObject);

    //        // Assert
    //        Assert.IsInstanceOfType(domainObject, typeof(Domain.Entities.League));
    //    }

    //    [TestMethod]
    //    public void Map_WithDtoObjects_ReturnsDomainObjects()
    //    {
    //        // Arrange
    //        var mapper = new LeagueMapper(null);

    //        // Act
    //        var domainObjects = mapper.Map(_dtoObjects);

    //        // Assert
    //        Assert.IsInstanceOfType(domainObjects, typeof(List<Domain.Entities.League>));
    //    }

    //    [TestMethod]
    //    public void Map_WithDomainObject_ReturnsDtoObject()
    //    {
    //        // Arrange
    //        var mapper = new LeagueMapper(null);

    //        // Act
    //        var dtoObject = mapper.Map(_domainObject);

    //        // Assert
    //        Assert.IsInstanceOfType(dtoObject, typeof(LeagueDto));
    //    }

    //    [TestMethod]
    //    public void Map_WithDomainObjects_ReturnsDtoObjects()
    //    {
    //        // Arrange
    //        var mapper = new LeagueMapper(null);

    //        // Act
    //        var dtoObjects = mapper.Map(_domainObjects);

    //        // Assert
    //        Assert.IsInstanceOfType(dtoObjects, typeof(List<LeagueDto>));
    //    }

    //    #endregion

    //}
}
