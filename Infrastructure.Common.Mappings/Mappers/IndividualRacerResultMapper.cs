﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Common.Mappings.Mappers
{
    public class OverallTeamResultsMapper : IMapper<Domain.Entities.OverallTeamResult, DistributedServices.Entities.OverallTeamResultDto>
    {
        public Domain.Entities.OverallTeamResult Map(DistributedServices.Entities.OverallTeamResultDto obj)
        {
            if (obj == null)
                return new Domain.Entities.OverallTeamResult();

            return new Domain.Entities.OverallTeamResult
            {
                LeagueName = obj.LeagueName,
                LeagueDesc = obj.LeagueDesc,
                LeagueId = obj.LeagueId,
                SeasonName = obj.SeasonName,
                SeasonDesc = obj.SeasonDesc,
                SeasonId = obj.SeasonId,
                TeamShortName = obj.TeamShortName,
                TeamFullName = obj.TeamFullName,
                TeamId = obj.TeamId,
                TotalPoints = obj.TotalPoints,
                ClientToken = Guid.Parse(obj.ClientToken)
            };
        }

        public DistributedServices.Entities.OverallTeamResultDto Map(Domain.Entities.OverallTeamResult obj)
        {
            if (obj == null)
                return new DistributedServices.Entities.OverallTeamResultDto();

            return new DistributedServices.Entities.OverallTeamResultDto
            {
                LeagueName = obj.LeagueName,
                LeagueDesc = obj.LeagueDesc,
                LeagueId = obj.LeagueId,
                SeasonName = obj.SeasonName,
                SeasonDesc = obj.SeasonDesc,
                SeasonId = obj.SeasonId,
                TeamShortName = obj.TeamShortName,
                TeamFullName = obj.TeamFullName,
                TeamId = obj.TeamId,
                TotalPoints = obj.TotalPoints,
                ClientToken = obj.ClientToken.ToString()
            };
        }


        public List<Domain.Entities.OverallTeamResult> Map(List<DistributedServices.Entities.OverallTeamResultDto> objs)
        {
            if (objs == null)
                return new List<Domain.Entities.OverallTeamResult>();

            var items = objs.Select(obj => new Domain.Entities.OverallTeamResult
            {
                LeagueName = obj.LeagueName,
                LeagueDesc = obj.LeagueDesc,
                LeagueId = obj.LeagueId,
                SeasonName = obj.SeasonName,
                SeasonDesc = obj.SeasonDesc,
                SeasonId = obj.SeasonId,
                TeamShortName = obj.TeamShortName,
                TeamFullName = obj.TeamFullName,
                TeamId = obj.TeamId,
                TotalPoints = obj.TotalPoints,
                ClientToken = Guid.Parse(obj.ClientToken)
            });

            return items.ToList();
        }

        public List<DistributedServices.Entities.OverallTeamResultDto> Map(List<Domain.Entities.OverallTeamResult> objs)
        {
            if (objs == null)
                return new List<DistributedServices.Entities.OverallTeamResultDto>();

            var items = objs.Select(obj => new DistributedServices.Entities.OverallTeamResultDto
            {
                LeagueName = obj.LeagueName,
                LeagueDesc = obj.LeagueDesc,
                LeagueId = obj.LeagueId,
                SeasonName = obj.SeasonName,
                SeasonDesc = obj.SeasonDesc,
                SeasonId = obj.SeasonId,
                TeamShortName = obj.TeamShortName,
                TeamFullName = obj.TeamFullName,
                TeamId = obj.TeamId,
                TotalPoints = obj.TotalPoints,
                ClientToken = obj.ClientToken.ToString()
            });

            return items.ToList();
        }
    }
}
