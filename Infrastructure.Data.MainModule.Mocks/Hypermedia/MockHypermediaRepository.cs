﻿using Infrastructure.Data.MainModule;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Infrastructure.Data.MainModule.Mocks.Hypermedia
{
    //public class MockLeagueRepository : ILeagueRepository
    //{
    //    private static List<Domain.Entities.League> leagues = new List<Domain.Entities.League>
    //        {
    //            new Domain.Entities.League 
    //            {
    //                ClientToken = Guid.NewGuid(),
    //                DateCreated = DateTime.Now,
    //                Description = "Test Description1",
    //                Id = 1,
    //            },
    //            new Domain.Entities.League 
    //            {
    //                ClientToken = Guid.NewGuid(),
    //                DateCreated = DateTime.Now,
    //                Description = "Test Description2",
    //                Id = 2,
    //            }
    //        };

    //    public List<Domain.Entities.League> GetManyBy(Func<Domain.Entities.League, bool> predicate)
    //    {
    //        return leagues;
    //    }

    //    public Domain.Entities.League GetBy(Func<Domain.Entities.League, bool> predicate)
    //    {
    //        return leagues.First();
    //    }

    //    public Domain.Entities.League Add(Domain.Entities.League item)
    //    {
    //        leagues.Add(item);

    //        return item;
    //    }

    //    public Domain.Entities.League Update(Domain.Entities.League item)
    //    {
    //        var leagueToUpdate = leagues.FirstOrDefault(i => i.Id == item.Id);

    //        leagues.Remove(leagueToUpdate);

    //        leagues.Add(item);

    //        return item;
    //    }

    //    public Domain.Entities.League Delete(int id)
    //    {
    //        var leagueToRemove = leagues.FirstOrDefault(i => i.Id == id);

    //        leagues.Remove(leagueToRemove);

    //        return leagueToRemove;
    //    }

    //    public IList<Domain.Entities.League> GetAll(bool isPublic)
    //    {
    //        return leagues;
    //    }
    //}
}
