﻿using Domain.Entities;
using Infrastructure.Data.MainModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Data.MainModule.Repositories
{
    public class IndividualRacerResultRepository : IIndividualRacerResultRepository
    {
        private readonly LeagueContext _context;

        public IndividualRacerResultRepository()
        {
            _context = new LeagueContext();
        }

        public IndividualRacerResult Add(IndividualRacerResult item)
        {
            return null;
        }

        public IndividualRacerResult Update(IndividualRacerResult item)
        {
            return null;
        }

        public IndividualRacerResult GetBy(Func<IndividualRacerResult, bool> predicate)
        {
            var items = _context.IndividualRacerResults.FirstOrDefault(predicate);

            return items;
        }

        public IEnumerable<IndividualRacerResult> GetAll()
        {
            return null;
        }

        public List<IndividualRacerResult> GetManyBy(Func<IndividualRacerResult, bool> predicate)
        {
            var items = _context.IndividualRacerResults.Where(predicate);

            return items.ToList();
        }

        public IndividualRacerResult Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}
