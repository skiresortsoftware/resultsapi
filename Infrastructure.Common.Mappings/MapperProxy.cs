﻿using AutoMapper;

namespace Infrastructure.Common.Mappings
{
	public class MapperProxy : IMapperProxy
	{
		public TResult Map<TSource,TResult>(TSource obj) where TSource : class
		{
			Mapper.CreateMap<TSource, TResult>();

			//Mapper.CreateMap<IList<TSource>, IList<TResult>>();

			var mappedItem = Mapper.Map<TSource, TResult>(obj);

			return mappedItem;
		}
	}
}
