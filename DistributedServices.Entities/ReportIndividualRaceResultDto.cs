﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistributedServices.Entities
{
    public class ReportIndividualRaceResultDto
    {
        public string LeagueName { get; set; }
        public string LeagueDesc { get; set; }
        public string SeasonName { get; set; }
        public string SeasonDesc { get; set; }
        public string RaceName { get; set; }
        public string RaceDescription { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Gender { get; set; }
        public int RacerId { get; set; }
        public int? RaceAge { get; set; }
        public int RaceId { get; set; }
        public double Time { get; set; }
        public double? Handicap { get; set; }
        public string Discipline { get; set; }
        public double? TeamPoints { get; set; }
        public double Points { get; set; }
    }
}
