﻿using Infrastructure.Data.MainModule.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Results
{
    public class ResultsService : IResultsService
    {
        public readonly ITeamRaceResultRepository _teamRaceResultsRepository;

        public readonly IOverallIndividualResultRepository _overallIndividualResultRepository;

        public readonly IOverallTeamResultRepository _overallTeamResultRepository;

        public readonly IIndividualRacerResultRepository _individualRacerResultRepository;

        public ResultsService(ITeamRaceResultRepository teamRaceResultsRepository,
                              IOverallIndividualResultRepository overallIndividualResultRepository,
                              IOverallTeamResultRepository overallTeamResultRepository,
                              IIndividualRacerResultRepository individualRacerResultRepository)
        {
            _teamRaceResultsRepository = teamRaceResultsRepository;
            _overallIndividualResultRepository = overallIndividualResultRepository;
            _overallTeamResultRepository = overallTeamResultRepository;
            _individualRacerResultRepository = individualRacerResultRepository;
        }


        public List<Domain.Entities.OverallTeamResult> GetOverallTeamResults(int seasonId)
        {
            var items = _overallTeamResultRepository.GetManyBy(i => i.SeasonId == seasonId);

            return items;
        }

        public List<Domain.Entities.OverallIndividualResult> GetOverallIndividualResults(int seasonId)
        {
            var items = _overallIndividualResultRepository.GetManyBy(i => i.SeasonId == seasonId);

            return items;
        }

        public List<Domain.Entities.IndividualRacerResult> GetIndividualRacerResults(int raceId)
        {
            var items = _individualRacerResultRepository.GetManyBy(i => i.RaceId == raceId);

            return items;
        }

        public List<Domain.Entities.TeamRaceResult> GetTeamRaceResults(int raceId)
        {
            var items = _teamRaceResultsRepository.GetManyBy(i => i.RaceId == raceId);

            return items;
        }

        public List<Domain.Entities.TeamRaceResult> GetAllTeamRaceResults(int seasonId, int teamId)
        {
            var items = _teamRaceResultsRepository.GetManyBy(i => i.SeasonId == seasonId && i.TeamId == teamId);

            return items;
        }
    }
}
