﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Common.Mappings.Mappers
{
    public class OverallIndividualResultMapper : IMapper<Domain.Entities.OverallIndividualResult, DistributedServices.Entities.OverallIndividualResultDto>
    {
        public Domain.Entities.OverallIndividualResult Map(DistributedServices.Entities.OverallIndividualResultDto obj)
        {
            if (obj == null)
                return new Domain.Entities.OverallIndividualResult();

            return new Domain.Entities.OverallIndividualResult
            {
                LeagueName = obj.LeagueName,
                LeagueDesc = obj.LeagueDesc,
                LeagueId = obj.LeagueId,
                SeasonName = obj.SeasonName,
                SeasonDesc = obj.SeasonDesc,
                SeasonId = obj.SeasonId,
                TeamShortName = obj.TeamShortName,
                TeamFullName = obj.TeamFullName,
                TeamId = obj.TeamId,
                TotalPoints = obj.TotalPoints,
                ClientToken = Guid.Parse(obj.ClientToken)
            };
        }

        public DistributedServices.Entities.OverallIndividualResultDto Map(Domain.Entities.OverallIndividualResult obj)
        {
            if (obj == null)
                return new DistributedServices.Entities.OverallIndividualResultDto();

            return new DistributedServices.Entities.OverallIndividualResultDto
            {
                LeagueName = obj.LeagueName,
                LeagueDesc = obj.LeagueDesc,
                LeagueId = obj.LeagueId,
                SeasonName = obj.SeasonName,
                SeasonDesc = obj.SeasonDesc,
                SeasonId = obj.SeasonId,
                TeamShortName = obj.TeamShortName,
                TeamFullName = obj.TeamFullName,
                TeamId = obj.TeamId,
                TotalPoints = obj.TotalPoints,
                ClientToken = obj.ClientToken.ToString()
            };
        }


        public List<Domain.Entities.OverallIndividualResult> Map(List<DistributedServices.Entities.OverallIndividualResultDto> objs)
        {
            if (objs == null)
                return new List<Domain.Entities.OverallIndividualResult>();

            var items = objs.Select(obj => new Domain.Entities.OverallIndividualResult
            {
                LeagueName = obj.LeagueName,
                LeagueDesc = obj.LeagueDesc,
                LeagueId = obj.LeagueId,
                SeasonName = obj.SeasonName,
                SeasonDesc = obj.SeasonDesc,
                SeasonId = obj.SeasonId,
                TeamShortName = obj.TeamShortName,
                TeamFullName = obj.TeamFullName,
                TeamId = obj.TeamId,
                TotalPoints = obj.TotalPoints,
                ClientToken = Guid.Parse(obj.ClientToken)
            });

            return items.ToList();
        }

        public List<DistributedServices.Entities.OverallIndividualResultDto> Map(List<Domain.Entities.OverallIndividualResult> objs)
        {
            if (objs == null)
                return new List<DistributedServices.Entities.OverallIndividualResultDto>();

            var items = objs.Select(obj => new DistributedServices.Entities.OverallIndividualResultDto
            {
                LeagueName = obj.LeagueName,
                LeagueDesc = obj.LeagueDesc,
                LeagueId = obj.LeagueId,
                SeasonName = obj.SeasonName,
                SeasonDesc = obj.SeasonDesc,
                SeasonId = obj.SeasonId,
                TeamShortName = obj.TeamShortName,
                TeamFullName = obj.TeamFullName,
                TeamId = obj.TeamId,
                TotalPoints = obj.TotalPoints,
                ClientToken = obj.ClientToken.ToString()
            });

            return items.ToList();
        }
    }
}
