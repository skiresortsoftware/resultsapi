﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Data.MainModule.Repositories
{
    public interface IOverallTeamResultRepository : IRepository<OverallTeamResult>
    {
    }
}
